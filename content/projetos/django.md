+++
title = 'Pêndulo invertido'
date = 2024-10-12T21:10:01-03:00
draft = false
+++

Faça seu próprio pêndulo invertido com os seguintes passos:   

**1. Definição do Problema**
- **Objetivo:** Manter um pêndulo na posição vertical, equilibrado sobre um carrinho que se move em uma dimensão.
- **Componentes Principais:** Pêndulo (haste), carrinho, motor/atuador, sensores (para medir o ângulo do pêndulo e a posição do carrinho).

**2. Modelagem do Sistema**
- **Equações de Movimento:** Desenvolver as equações diferenciais que descrevem a dinâmica do sistema. Isso geralmente envolve a aplicação das leis de Newton ou a formulação de Lagrange.
- **Variáveis de Estado:** Definir as variáveis de estado (e.g., posição e velocidade do carrinho, ângulo e velocidade angular do pêndulo).

**3. Linearização**
- **Ponto de Operação:** Linearizar as equações de movimento ao redor do ponto de equilíbrio (pêndulo vertical).
- **Modelo Linearizado:** Obter o modelo linearizado em termos de uma representação de espaço de estado ou função de transferência.

**4. Projeto do Controlador**
- **Controlador PID:** Projetar um controlador PID (Proporcional-Integral-Derivativo) para estabilizar o pêndulo. Ajustar os ganhos do controlador (Kp, Ki, Kd) para obter a resposta desejada.
- **Controle de Estado:** Alternativamente, usar uma abordagem de controle de estado (por exemplo, feedback de estado completo ou LQR - Regulador Quadrático Linear).

**5. Simulação**
- **Simulador:** Implementar o modelo do sistema e o controlador em um ambiente de simulação (por exemplo, MATLAB/Simulink).
- **Validação:** Testar a estabilidade e a resposta do sistema no simulador, ajustando os parâmetros do controlador conforme necessário.

**6. Implementação Física**
- **Hardware:** Montar o pêndulo invertido físico, incluindo o carrinho, motor, sensores e microcontrolador (como um Arduino ou Raspberry Pi).
- **Software:** Programar o controlador no microcontrolador, utilizando dados dos sensores para calcular as ações de controle.
- **Teste:** Executar testes no sistema físico para verificar o desempenho e fazer ajustes finais.

**7. Refinamento e Otimização**
- **Ajustes Finos:** Refine os parâmetros do controlador com base nos testes físicos.
- **Robustez:** Verifique a robustez do sistema sob diferentes condições (por exemplo, variação de massa do pêndulo, atrito no carrinho).

**8. Documentação**
- **Relatório:** Documentar o processo de desenvolvimento, incluindo modelagem, projeto do controlador, simulações, implementação e resultados experimentais.
- **Manuais:** Criar manuais de operação e manutenção do sistema.
