
Seja bem vindo!  

Neste site, você encontrará:

- Minha biografia
- Meus projetos
- Meu currículo

----

## **Quem sou eu** 

Meu nome é Letícia Satie Ito, tenho 20 anos e sou estudante de engenharia mecatrônica na Escola Politécnica da USP.  
Nasci em São Paulo, mas moro em Embu das Artes. Tenho Ensino médio completo em escola particular.  
Alguns dos meus hobbies são cozinhar e assistir séries coreanas.   
Ultimamente tenho gostado de nadar e correr. Só falta o ciclismo para tentar entrar no triatlo.   
A minha família é a parte mais importante da minha vida. 
</article>