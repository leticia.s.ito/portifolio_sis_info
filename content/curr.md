+++
title = 'Currículo'
date = 2024-10-12T21:02:35-03:00
draft = false
+++

### Experiências
- **1° Lugar em Hackaton sobre cabines de avião**  
  Promovido pela Escola de Engenharia de São Carlos e patrocinado pela Embraer, o evento de 72 horas remotas tinha como objetivo apresentar soluções para melhorias em cabines executivas em formato de pitch.

- **Membro da Eletrônica do Grupo Zima**  
  Grupo de soluções médico-hospitalares que fornece soluções de engenharia para profissionais do Hospital Universitário.

### Competências
- Trabalho em grupo
- Persistência
- Interesse

### Habilidades
- Python
- Pacote Office
- Inglês
